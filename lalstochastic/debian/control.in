Source: lalstochastic
Section: science
Priority: optional
Maintainer: Steffen Grunewald <steffen.grunewald@aei.mpg.de>
Uploaders: Adam Mercer <adam.mercer@ligo.org>, GitLab <gitlab@git.ligo.org>
Build-Depends: debhelper (>= 9),
  dh-python,
  libmetaio-dev (>= 8.2),
  liboctave-dev,
  pkg-config,
  python-all-dev,
  python-numpy,
  python3-all-dev,
  python3-numpy,
  swig (>= 3.0.7),
  zlib1g-dev,
  lal-dev (>= @MIN_LAL_VERSION@~),
  lal-octave (>= @MIN_LAL_VERSION@~),
  python-lal (>= @MIN_LAL_VERSION@~),
  python3-lal (>= @MIN_LAL_VERSION@~),
  lalmetaio-dev (>= @MIN_LALMETAIO_VERSION@~),
  lalmetaio-octave (>= @MIN_LALMETAIO_VERSION@~),
  python-lalmetaio (>= @MIN_LALMETAIO_VERSION@~),
  python3-lalmetaio (>= @MIN_LALMETAIO_VERSION@~)
X-Python-Version: >= 2.7
X-Python3-Version: >= 3.4
Standards-Version: 3.9.8

Package: lalstochastic
Architecture: any
Depends: ${misc:Depends},
  ${shlibs:Depends},
  libmetaio1 (>= 8.2),
  lal (>= @MIN_LAL_VERSION@~),
  lalmetaio (>= @MIN_LALMETAIO_VERSION@~)
Description: LSC Algorithm Library Stochastic
 The LSC Algorithm Stochastic Library for gravitational wave data analysis.
 This package contains the shared-object libraries needed to run applications
 that use the LAL Stochastic library.

Package: lalstochastic-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
  ${shlibs:Depends},
  libmetaio-dev (>= 8.2),
  zlib1g-dev,
  lal-dev (>= @MIN_LAL_VERSION@~),
  lalmetaio-dev (>= @MIN_LALMETAIO_VERSION@~),
  lalstochastic (= ${binary:Version})
Description: LSC Algorithm Library Stochastic Developers
 The LSC Algorithm Stochastic Library for gravitational wave data analysis.
 This package contains files needed build applications that use the LAL
 Stochastic library.

Package: lalstochastic-python
Depends: python-lalstochastic, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 lalstochastic-python was renamed python-lalstochastic,
 this is a transitional package, it can safely be removed.

Package: python-lalstochastic
Section: python
Replaces: lalstochastic-python (<< 1.1.20-1~)
Breaks: lalstochastic-python (<< 1.1.20-1~)
Architecture: any
Depends: ${misc:Depends},
  ${python:Depends},
  ${shlibs:Depends},
  lal-python (>= @MIN_LAL_VERSION@~),
  lalmetaio-python (>= @MIN_LALMETAIO_VERSION@~),
  lalstochastic (= ${binary:Version})
Description: Python bindings for LALStochastic
 The LSC Algorithm Stochastic Library for gravitational wave data analysis.
 This package contains Python bindings for the LAL Stochastic library.

Package: lalstochastic-python3
Depends: python3-lalstochastic, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 lalstochastic-python3 was renamed python3-lalstochastic,
 this is a transitional package, it can safely be removed.

Package: python3-lalstochastic
Section: python
Replaces: lalstochastic-python3 (<< 1.1.20-1~)
Breaks: lalstochastic-python3 (<< 1.1.20-1~)
Architecture: any
Depends: ${misc:Depends},
  ${python3:Depends},
  ${shlibs:Depends},
  python3-lal (>= @MIN_LAL_VERSION@~),
  python3-lalmetaio (>= @MIN_LALMETAIO_VERSION@~),
  lalstochastic (= ${binary:Version})
Description: Python 3 bindings for LALStochastic
 The LSC Algorithm Stochastic Library for gravitational wave data analysis.
 This package contains Python 3 bindings for the LAL Stochastic library.

Package: lalstochastic-octave
Architecture: any
Depends: ${misc:Depends},
  ${shlibs:Depends},
  octave,
  lal-octave (>= @MIN_LAL_VERSION@~),
  lalmetaio-octave (>= @MIN_LALMETAIO_VERSION@~),
  lalstochastic (= ${binary:Version})
Description: Octave bindings for LALStochastic
 The LSC Algorithm Stochastic Library for gravitational wave data analysis.
 This package contains Octave bindings for the LAL Stochastic library.
