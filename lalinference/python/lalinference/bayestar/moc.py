import warnings
from .. import distance
from ..moc import *
__all__ = moc.__all__

warnings.warn('lalinference.bayestar.moc is deprecated, use lalinference.moc instead', DeprecationWarning)
